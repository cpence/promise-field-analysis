# Text Mining in Scientific Fields

This repository contains the raw data that supports two small analyses that I
published in chapter 8 of my book _Integrative Promise._

## Scopus Search

All search results reported here were performed using UCLouvain's Scopus
subscription on April 17, 2023.

```
TITLE-ABS-KEY ( "NLP"  OR  "natural language processing"  OR  "text mining" )
```

This set of keywords is drawn from the methodology at
<https://royalsocietypublishing.org/doi/10.1098/rspb.2021.2721>.

Results: 129,287 documents

### Subject Area

Scopus tags each journal with one or more "subject area" tags that describe its
content. These are immediately useful for understanding the disciplines that
these text mining papers involve. Notably, most of them are published in
journals tagged with the subject areas "computer science", "engineering", or
"mathematics", disciplines where NLP algorithms are developed directly.

In what follows, I leave these three tags aside. I also do _not_ make any effort
to account for the fact that some journals are tagged with multiple subject
areas. The

- Social Sciences (21784)
- Arts and Humanities (14555)
- Medicine (12418)
- Decision Sciences (9770)
- Business, Management and Accounting (5287)
- Physics and Astronomy (4157)
- Biochemistry, Genetics and Molecular Biology (4048)
- Materials Science (2862)
- Energy (2454)
- Health Professions (2323)
- Environmental Science (2062)
- Chemical Engineering (1913)
- Neuroscience (1521)
- Psychology (1352)
- Agricultural and Biological Sciences (1231)
- Multidisciplinary (1188)
- Chemistry (1078)
- Economics, Econometrics and Finance (976)
- Earth and Planetary Sciences (882)
- Pharmacology, Toxicology and Pharmaceutics (652)
- Immunology and Microbiology (473)
- Nursing (425)

The high representation of the social sciences and humanities is quickly
attributable to the study of "big data" and social media corpora directly; such
journals (like _Social Network Analysis and Mining_ or _Scientific Data_) are
tagged with these tags.

I then group "life" versus "physical" sciences in the following way:

- Life Sciences (26505)
  - Medicine; Biochemistry, Genetics and Molecular Biology; Health Professions;
    Environmental Science; Neuroscience; Psychology; Agricultural and Biological
    Sciences; Pharmacology, Toxicology and Pharmaceutics; Immunology and
    Microbiology; Nursing
- Physical Sciences (13346)
  - Physics and Astronomy; Materials Science; Energy; Chemical Engineering;
    Chemistry; Earth and Planetary Sciences

This figure was used to justify the chapter's claim that the life sciences seem
to engage in text mining at a rate about twice as frequent as the physical
sciences, and somewhat more frequently than the social sciences.

### Graphs over Time

These searches were carried out a few days later, on April 27, 2023.

I then ran the same search above, now limiting it to life sciences and physical
sciences (as defined by the sets of subject tags above), respectively. For life
science (20,561 documents):

```
TITLE-ABS-KEY ( "NLP"  OR  "natural language processing"  OR  "text mining" )  AND  ( LIMIT-TO ( SUBJAREA ,  "MEDI" )  OR  LIMIT-TO ( SUBJAREA ,  "BIOC" )  OR  LIMIT-TO ( SUBJAREA ,  "HEAL" )  OR  LIMIT-TO ( SUBJAREA ,  "ENVI" )  OR  LIMIT-TO ( SUBJAREA ,  "NEUR" )  OR  LIMIT-TO ( SUBJAREA ,  "PSYC" )  OR  LIMIT-TO ( SUBJAREA ,  "AGRI" )  OR  LIMIT-TO ( SUBJAREA ,  "PHAR" )  OR  LIMIT-TO ( SUBJAREA ,  "IMMU" )  OR  LIMIT-TO ( SUBJAREA ,  "NURS" ) )
```

For physical sciences (10,209 documents):

```
TITLE-ABS-KEY ( "NLP"  OR  "natural language processing"  OR  "text mining" )  AND  ( LIMIT-TO ( SUBJAREA ,  "PHYS" )  OR  LIMIT-TO ( SUBJAREA ,  "MATE" )  OR  LIMIT-TO ( SUBJAREA ,  "ENER" )  OR  LIMIT-TO ( SUBJAREA ,  "CENG" )  OR  LIMIT-TO ( SUBJAREA ,  "CHEM" )  OR  LIMIT-TO ( SUBJAREA ,  "EART" ) )
```

Selecting "View All" under "Year" for these searches allowed me to plot these
queries over time, used to create the graph figure present here and published in
the book. I ignored the data for 2023, as the year was in progress.

The TSV file in this folder contains that data, with years in column 1, life
science in column 2, and physical science in column 3.

set term pdfcairo enhanced mono lw 3 font "Myriad Pro,24" size 11,6
set output 'nlp-over-time.pdf'

#set term pngcairo enhanced mono lw 3 font "Myriad Pro,18" size 1100,600
#set output 'nlp-over-time.png'

set xlabel "Year"
set ylabel "Articles using NLP/text mining"
set autoscale
set key left top

plot 'nlp-over-time.tsv' using 1:2 with lines t "Life Sciences", \
     'nlp-over-time.tsv' using 1:3 with lines t "Physical Sciences" dashtype "."
